import Airtable, {Table} from 'airtable';
import { Tip } from '../entities';
import {Post} from '../entities/Post';
import {Topic} from '../entities/Topic';

const API_KEY = 'keym8jGqDxVwLQG51';

export class Database {
    private tips: Table<Tip>;
    private posts: Table<Post>;

    constructor() {
        const airtable: Airtable.Base = new Airtable({
            apiKey: API_KEY,
            endpointUrl: 'https://api.airtable.com',
        }).base('appXGVIkQI1zDUedj');

        this.tips = airtable('Tips') as Table<Tip>;
        this.posts = airtable('Communityposts') as Table<Post>;
    }

    public getTipOfTheDay(): Promise<Tip | undefined> {
        return this.tips.select().all()
            .then((response) => {
                return response.map(({fields}) => {
                    const images = fields.image as ReadonlyArray<Airtable.Attachment>;
                    if (images) {
                        fields.image = images[0].url;
                    }
                    return fields;
                })
                    .find((tip) => tip.name === 'Tip of the day');
            });
    }

    public getThisWeekPosts(): Promise<Topic> {
        const topic = 'This week\'s topics';
        return this.posts.select().all()
            .then((response) => {
                const posts = response.map(({fields}) => fields)
                    .filter((post) => {
                        console.log(post);
                        return post.topic === topic;
                    });
                return {
                    name: topic,
                    posts,
                };
            });
    }
}
