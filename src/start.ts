
// Start the server
import app from './Server';
import { logger } from './shared';
import {Database} from './shared/Database';

const port = Number(process.env.PORT || 3000);
app.listen(port, () => {
    logger.info('Express server started on port: ' + port);
});
