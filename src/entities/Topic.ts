import {Post} from './Post';

export interface Topic {
    name: string;
    posts: Post[];
}
