export interface Tip extends Airtable.FieldSet {
    name: string;
    title: string;
    intro: string;
    text: string;
    image: string | ReadonlyArray<Airtable.Attachment>;
    createdTimestamp: string;
}
