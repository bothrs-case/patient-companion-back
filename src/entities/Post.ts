export interface Post extends Airtable.FieldSet {
    topic: string;
    createdTimestamp: string;
    title: string;
    text: string;
    author: string;
}
