import { Router } from 'express';
import ApiRouter from './Api';

// Init router and path
const router = Router();

// Add sub-routes
router.use('/', ApiRouter);

// Export the base-router
export default router;
