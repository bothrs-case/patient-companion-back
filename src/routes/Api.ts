
import { Request, Response, Router } from 'express';
import { OK, INTERNAL_SERVER_ERROR } from 'http-status-codes';
import {Database} from '../shared/Database';
import {logger} from '../shared';

// Init shared
const router = Router();

/******************************************************************************
 *                    Get - "GET /api/tips/tip-of-the-day"
 ******************************************************************************/

router.get('/tips/tip-of-the-day', async (req: Request, res: Response) => {

    try {
        const tip = await new Database().getTipOfTheDay();
        return res.status(OK).json(tip);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(INTERNAL_SERVER_ERROR).json({
            error: err.message,
        });
    }
});

/******************************************************************************
 *                    Get - "GET /api/topics/this-week"
 ******************************************************************************/

router.get('/topics/this-week', async (req: Request, res: Response) => {

    try {
        const topic = await new Database().getThisWeekPosts();
        return res.status(OK).json(topic);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(INTERNAL_SERVER_ERROR).json({
            error: err.message,
        });
    }
});



/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
